# QuartzEngine
## Current Version: v0.1.0 Alpha

A Java Game Engine based on LWJGL3!  It provides an easy, non-mess wrapper around LWJGL for getting straight to the development!

Using QuartzEngine can significantly reduce setup for Java LWJGL3 games.  It supports both 2D and 3D games.

Current Features:

• Easy OpenGL Shader Implementation

• Texture import to OpenGL

• Multiple language support

• Fancy Logger

and many more!

Find it at: http://ohaizac.github.io/QuartzEngine
